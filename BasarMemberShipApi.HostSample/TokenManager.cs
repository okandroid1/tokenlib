﻿using BasarMemberShipApi.SqlDataAccess;
using BasarMemberShipApi.TokenLibrary;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BasarMemberShipApi.HostSample
{
    public static class TokenManager
    {
        private static readonly TokenOperations t;
        
        // Sql İşlemleri

        static TokenManager()
        {
            SqlDbSettings setting = new SqlDbSettings("192.168.1.110", "murcell", "sa", "mg2921101.MG");
            SqlOperations op = new SqlOperations(setting);

            //op.CreateUserTable();
            //op.CreateTokenTable();
            t = new TokenOperations(op);            
        }


        // Burası mongo DB işlemleri için açılacak

        //static TokenManager()
        //{
        //    MongoDbSettings settings = new MongoDbSettings();
        //    settings.Server = "127.0.0.1";
        //    settings.UserName = "";
        //    settings.Password = "";
        //    settings.DatabaseName = "GM";
        //    settings.Port = "27017";

        //    t = new TokenOperations(new MongoOperations(settings));
        //}

        public static TokenOperations Get()
        {
            return TokenManager.t;
        }
    }
}