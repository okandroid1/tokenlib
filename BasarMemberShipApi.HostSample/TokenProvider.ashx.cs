﻿using BasarMemberShipApi.TokenLibrary;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace BasarMemberShipApi.HostSample
{
    /// <summary>
    /// Summary description for TokenProvider
    /// </summary>
    public class TokenProvider : HttpTaskAsyncHandler
    {
        public async override Task ProcessRequestAsync(HttpContext context)
        {
            string[] header = context.Request.Headers.AllKeys;

            if (header.Contains("UserName") && header.Contains("Password") && header.Contains("ExpDate") && header.Contains("AppType"))
            {
                string userName = context.Request.Headers.Get("UserName");
                string passWord = context.Request.Headers.Get("Password");

                // decode
                string appType = context.Request.Headers.Get("AppType");
                int expDate = int.Parse(context.Request.Headers.Get("ExpDate"));

                var tm = TokenManager.Get();

                var token = tm.GetToken(userName, passWord, appType, expDate);

                if (token != null)
                {
                    BsonDocument doc = new BsonDocument()
                    {
                        { "token", token.Token.ToString() },
                        { "expireAt", token.Duration }
                    };

                    await W3Utils.PostJson(context, doc);
                }
                else
                {
                    BsonDocument doc = new BsonDocument()
                    {
                        { "error", "invalid username/password" }
                    };

                    await W3Utils.PostJson(context, doc);
                }
            }
            else
            {
                BsonDocument doc = new BsonDocument()
                {
                    { "error", "..." }
                };

                await W3Utils.PostJson(context, doc);
            }
        }
    }
}