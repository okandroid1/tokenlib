﻿using BasarMemberShipApi.TokenLibrary;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace BasarMemberShipApi.HostSample
{
    public abstract class BaseHandler : HttpTaskAsyncHandler
    {
        public async override Task ProcessRequestAsync(HttpContext context)
        {
            string[] header = context.Request.Headers.AllKeys;

            // Eğer Headerda Token varsa aşağıdaki kod satırı çalışsın
            if (header.Contains("Token"))
            {
                string token = context.Request.Headers.Get("Token");
                //await ValidateToken(token, context);

                var tk = TokenManager.Get();
                TokenInfo ti = tk.IsAuth(token);

                if (ti != null)
                {
                    //tk.AddTokenToDb(ti);
                    context.Response.Headers.Add("sonuc", "1");
                    BsonDocument doc = new BsonDocument()
                    {
                        { "sonuc", "1" }
                    };

                    await W3Utils.PostJson(context, doc);
                }
                else
                {
                    BsonDocument doc = new BsonDocument()
                    {
                        { "sonuc", "0" }
                    };

                    await W3Utils.PostJson(context, doc);
                }
            }
            else
            {
                BsonDocument doc = new BsonDocument()
                {
                    { "sonuc", "0" }
                };

                await W3Utils.PostJson(context, doc);
            }
        }

        //public abstract void Execute() { }

    }
}