var Token = (function (t) {

    var currentToken;
    var tajaxSettings = {};

    t.getToken = function () {
        return currentToken;
    };

    t.setToken = function (val) {
        currentToken = val;
    };

    t.tajaxSetup = function (settings) {
        tajaxSettings = settings;
    };

    t.tajax = function (settings) {
        settings = settings || {};
        settings.type = settings.type || 'POST';

        settings.beforeSend = function (settings) {
            settings.setRequestHeader("Token", Token.getToken());
        }

        return jQuery.ajax(settings).done(function (data, status, xhr) {

            if (data.result == "0") {
                if ((tajaxSettings.onInvalidToken) && (typeof tajaxSettings.onInvalidToken == "function")) {
                    tajaxSettings.onInvalidToken(data);
                }
            };

        });
    };

    return t;
}(Token || {}));
