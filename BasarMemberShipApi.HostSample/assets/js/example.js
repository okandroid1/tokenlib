﻿function createToken() {
    $("#msg").html('');
    $.ajax({
        type: 'POST',
        url: 'http://localhost/BasarMemberShipApi/TokenProvider.ashx',
        //url: 'http://localhost:14525/TokenProvider.ashx',
        headers: {
            "UserName": $("#username").val(),
            "Password": $("#password").val(),
            "AppType": "Web",
            "ExpDate": $("#timeOut").val()
        }
    }).done(function (data) {

        // Giriş yapılırken bilgiler doğrulandıysa....
        if (data.token) {

            Token.setToken(data.token);

            localStorage.setItem("Token", Token.getToken());


            var Wrapper = $("<div class='alert alert-success'></div>");
            Wrapper.html('Bilgileriniz doğru, yönlendiriliyorsunuz...');

            $("#GeomarketLogin #resultWrapper").html(Wrapper);

            $("#token").val(data.token);
            Token.setToken(data.token);

        }
            // Giriş yapılırken bilgiler yanlışsa...
        else {
            var Wrapper = $("#msg");
            Wrapper.html('Giriş Bilgileriniz Geçersiz...');

        }

    }).error(function (jqXHR, textStatus, errorThrown) { });
};

function validateToken() {

    Token.setToken($("#token").val());

    Token.tajax({ url: "http://localhost/BasarMemberShipApi/TokenValidator.ashx" }).done(function (data, status, xhr) {
        if (xhr.getResponseHeader('sonuc') == 1) {
            $("#msg").html("Valid Token !");
        }
        else {
            $("#msg").html("Invalid Token !");
        }
    }).error(function (data, status, xhr) {
        $("#msg").html("Invalid Token !");
    });

}