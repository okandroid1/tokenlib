﻿using BasarMemberShipApi.SqlDataAccess.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasarMemberShipApi.SqlDataAccess
{
    public class SqlHelper
    {
        //private SqlConnection conn = new SqlConnection("data source=192.168.1.110;initial catalog=GlobalAuthDB;persist security info=True;user id=sa;password=mg2921101.MG");
        private SqlConnection conn;

        public SqlHelper(SqlConnection connectionString)
        {
            conn = connectionString;
        }

        protected void sqlcontrol()
        {
            try { conn.Close(); }
            catch { }
        }


        public bool CreateTable()
        {
            string commandText = "CREATE TABLE Users" + "(UserId INTEGER CONSTRAINT UserId PRIMARY KEY," + "UserName nvarchar(100), Password nvarchar(50))";
            SqlCommand cmd = new SqlCommand(commandText, conn);
            try
            {
                cmd.ExecuteNonQuery();
                // Adding records the table
                string cmdText = "INSERT INTO Users(UserId, UserName, Password) " +
                "VALUES (100, 'admin', '12345')";
                cmd = new SqlCommand(cmdText, conn);
                cmd.ExecuteNonQuery();
                return true;
            }
            catch (SqlException ae)
            {
                return false;
            }
        }
        


        public DataTable KayıtGetir(string tableName)
        {
            sqlcontrol();
            conn.Open();
            SqlDataAdapter adap = new SqlDataAdapter("select id,ad,soyad,tel from "+tableName+"", conn);
            DataTable tbl = new DataTable();
            adap.Fill(tbl);
            conn.Close();
            return tbl;
        }

        //public void SatiriSil(string id, string tableName)
        //{
        //    sqlcontrol();
        //    conn.Open();
        //    SqlCommand kmt = new SqlCommand("DELETE " + tableName + " where id=" + ID, conn);
        //    kmt.ExecuteNonQuery();
        //    conn.Close();
        //}

        //public void SatiriGuncelle(string ID, string Yeni_ad, string Yeni_soyad, string Yeni_tel)
        //{
        //    sqlcontrol();
        //    conn.Open();
        //    SqlCommand kmt = new SqlCommand("UPDATE  tbKisiler set ad='" + Yeni_ad + "',soyad='" + Yeni_soyad + "',tel='" + Yeni_tel + "' where id=" + ID, conn);
        //    kmt.ExecuteNonQuery();
        //    conn.Close();
        //}

        public void KullaniciEkle(User user)
        {
            sqlcontrol();
            conn.Open();
            SqlCommand kmt = new SqlCommand("Insert into Users (UserName,Password) Values ('" + user.UserName + "','" + user.Password + "')", conn);
            kmt.ExecuteNonQuery();
            conn.Close();
        }

        public void TokenEkle(Token token)
        {
            sqlcontrol();
            conn.Open();
            SqlCommand kmt = new SqlCommand("Insert into  Tokens (Token, UserName, Duration, CreatedDate) Values ('" + token.TokenNo + "','" + token.UserName + "','" + token.Duration + "','" + token.CreatedDate + "')", conn);
            kmt.ExecuteNonQuery();
            conn.Close();
        }

    }
}
