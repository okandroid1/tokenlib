﻿using BasarMemberShipApi.SqlDataAccess.Data;
using BasarMemberShipApi.TokenLibrary;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasarMemberShipApi.SqlDataAccess
{
    public class SqlOperations : IDbOperations<TokenInfo>
    {
        private SqlConnection conn;

        private SqlDbSettings settings;

        public SqlOperations(SqlDbSettings settings)
        {
            this.settings = settings;
            conn = new SqlConnection(settings.ConnectionString);
        }

        protected void SqlControl()
        {
            try { conn.Close(); }
            catch { }
        }

        public List<TokenInfo> GetTokenList()
        {
            DataTable tokenList = TokenGetir();

            List<TokenInfo> tkList = new List<TokenInfo>();

            foreach (DataRow item in tokenList.Rows)
            {
                tkList.Add(new TokenInfo
                {
                    Id = int.Parse(item.ItemArray[0].ToString()),
                    Token = (ShortGuid)item.ItemArray[1].ToString(),
                    UserName = item.ItemArray[2].ToString(),
                    Duration = int.Parse(item.ItemArray[3].ToString()),
                    CreatedDate = (DateTime)item.ItemArray[4]
                });
            }

            return tkList;
        }

        public void AddToken(TokenInfo tokeninfo)
        {
            TokenEkle(tokeninfo);
        }

        
        public bool ValidateUser(string usr, string pwd)
        {
            SqlControl();
            conn.Open();
            SqlDataAdapter adap = new SqlDataAdapter("select * from Users where UserName='"+usr+"' and Password='"+pwd+"'", conn);
            DataTable tbl = new DataTable();
            adap.Fill(tbl);
            
            if (tbl.Rows.Count>0)
            {
                return true;
                conn.Close();
            }
            else
            {
                return false;
            }         
        }

        private DataTable TokenGetir()
        {
            SqlControl();
            conn.Open();
            SqlDataAdapter adap = new SqlDataAdapter("select Id,Token,UserName,Duration,CreatedDate from Tokens", conn);
            DataTable tbl = new DataTable();
            adap.Fill(tbl);
            conn.Close();
            return tbl;
        }

        private void KullaniciEkle(User user)
        {
            SqlControl();
            conn.Open();
            SqlCommand kmt = new SqlCommand("Insert into Users (UserName,Password) Values ('" + user.UserName + "','" + user.Password + "')", conn);
            kmt.ExecuteNonQuery();
            conn.Close();
        }

        private void TokenEkle(TokenInfo token)
        {
            SqlControl();
            conn.Open();
            SqlCommand kmt = new SqlCommand("Insert into Tokens (Token, UserName, Duration, CreatedDate) Values ('" + token.Token + "','" + token.UserName + "','" + token.Duration + "','" + token.CreatedDate + "')", conn);
            kmt.ExecuteNonQuery();
            conn.Close();
        }

        public bool CreateUserTable()
        {

            SqlControl();
            conn.Open();

            DataTable t = conn.GetSchema("Tables");

            string commandText = "CREATE TABLE Users" + "(UserId INTEGER CONSTRAINT UserId PRIMARY KEY," + "UserName nvarchar(100), Password nvarchar(50))";

            SqlCommand cmd = new SqlCommand(commandText, conn);

            try
            {
                if (t!=null)
                {
                    cmd.ExecuteNonQuery();
                    // Adding records the table
                    string cmdText = "INSERT INTO Users(UserId, UserName, Password) " +
                    "VALUES (100, 'admin', '12345') ";
                    cmd = new SqlCommand(cmdText, conn);
                    cmd.ExecuteNonQuery();
                    return true;
                }
                else
                {
                    return false;
                }               

            }
            catch (SqlException ae)
            {
                return false;

            }
        }

        public bool CreateTokenTable()
        {
            string commandText = "CREATE TABLE Tokens" + "(Id int not null identity(1, 1) CONSTRAINT Id PRIMARY KEY ," + "Token nvarchar(200), UserName nvarchar(150), Duration int, CreatedDate datetime2(7))";
            
            SqlCommand cmd = new SqlCommand(commandText, conn);
            DataTable t = conn.GetSchema("Tables");

            foreach (DataRow item in t.Rows)
            {
                var deneme = item;
            }

            try
            {
                if (t!=null)
                {
                    cmd.ExecuteNonQuery();
                    return true;
                }
                else
                {
                    return false;
                }
                

            }
            catch (SqlException ae)
            {
                return false;

            }

            //try
            //{
            //    cmd.ExecuteNonQuery();
            //    // Adding records the table
            //    string cmdText = "INSERT INTO Tokens(Id, Token, UserName, Duration, CreatedDate) " +
            //    "VALUES (1, 'admin', '12345') ";
            //    cmd = new SqlCommand(cmdText, conn);
            //    cmd.ExecuteNonQuery();
            //    return true;

            //}
            //catch (SqlException ae)
            //{
            //    return false;
            //    // MessageBox.Show(ae.Message.ToString());
            //}
        }
       
    }
}
