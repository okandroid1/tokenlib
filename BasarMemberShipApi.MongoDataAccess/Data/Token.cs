﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasarMemberShipApi.MongoDataAccess.Data
{
    public class Token
    {
        public int Id { get; set; }
        public string TokenNo { get; set; }
        public string UserName { get; set; }
        public int Duration { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
