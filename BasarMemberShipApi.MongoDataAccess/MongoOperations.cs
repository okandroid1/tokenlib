﻿using BasarMemberShipApi.TokenLibrary;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasarMemberShipApi.MongoDataAccess
{
    public class MongoOperations : IDbOperations<TokenInfo>
    {
        private MongoDatabase _db;

        public MongoOperations(MongoDbSettings setting)
        {

            var conStr = "";

            if (setting.UserName == "" || setting.Password == "")
            {
                conStr = "mongodb://" + setting.Server + ":" + setting.Port + "/" + setting.DatabaseName + "";
            }
            else
            {
                conStr = "mongodb://" + setting.UserName + ":" + setting.Password + "@" + setting.Server + ":" + setting.Port + "/" + setting.DatabaseName + "";
            }

            var client = new MongoClient(conStr);
            MongoServer _server = client.GetServer();

            _db = _server.GetDatabase(setting.DatabaseName);
        }

        public List<TokenInfo> GetTokenList()
        {

            var tkList = _db.GetCollection<BsonDocument>("TOKENS").FindAll().ToList();
            List<TokenInfo> tokenList = new List<TokenInfo>();

            foreach (var item in tkList)
            {
                tokenList.Add(new TokenInfo
                {
                    Token = Convert.ToString(item["Token"]),
                    UserName = Convert.ToString(item["UserName"]),
                    Duration = Convert.ToInt32(item["Duration"]),
                    CreatedDate = Convert.ToDateTime(item["CreatedDate"])

                });
            }

            return tokenList;
        }

        public void AddToken(TokenInfo tokeninfo)
        {
            BsonDocument doc = new BsonDocument()
                {
                    { "Token", Convert.ToString(tokeninfo.Token) },
                    { "Duration",  tokeninfo.Duration  },
                    { "UserName",  tokeninfo.UserName  },
                    { "CreatedDate",  DateTime.Now },
                };

            var col = _db.GetCollection<BsonDocument>("TOKENS");

            col.Insert(doc);

        }

        public bool ValidateUser(string usr, string pwd)
        {


            var QUERY = Query.And(Query.EQ("UserName", usr), Query.EQ("Password", pwd));

            var result = _db.GetCollection<BsonDocument>("USERS").FindOne(QUERY);

            if (result != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
