﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasarMemberShipApi.MongoDataAccess
{
    public class MongoDbSettings
    {
        public string Server = "127.0.0.1";
        public string DatabaseName = "GM";
        public string UserName = "";
        public string Password = "";
        public string Port = "27017";

    }
}
