﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasarMemberShipApi.TokenLibrary
{
    public interface IDbOperations<T> where T : class
    {
        List<T> GetTokenList();
        void AddToken(TokenInfo tokeninfo);
        bool ValidateUser(string usr, string pwd);
    }
}
