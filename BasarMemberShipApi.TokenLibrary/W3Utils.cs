﻿using MongoDB.Bson;
using MongoDB.Bson.IO;
using System.Threading.Tasks;
using System.Web;

namespace BasarMemberShipApi.TokenLibrary
{
    public static class W3Utils
    {
        private static JsonWriterSettings writerSettings;

        static W3Utils()
        {
            writerSettings = JsonWriterSettings.Defaults.Clone();
            writerSettings.GuidRepresentation = GuidRepresentation.CSharpLegacy;
            writerSettings.Indent = false;
            writerSettings.MaxSerializationDepth = 100;
            writerSettings.NewLineChars = "";
            writerSettings.OutputMode = JsonOutputMode.Strict;
        }

        public static async Task PostJson(HttpContext context, BsonDocument json)
        {
            await PostJson(context, json.ToJson(writerSettings));
        }

        public static async Task PostJson(HttpContext context, string json)
        {
            string callback = context.Request.QueryString["callback"];

            context.Response.ContentType = "application/json";
            context.Response.Charset = "utf-8";

            if (string.IsNullOrEmpty(callback))
            {
                context.Response.Write(json);
            }
            else
            {
                context.Response.Write(callback);
                context.Response.Write("(");
                context.Response.Write(json);
                context.Response.Write(");");
            }
        }
    }
}
