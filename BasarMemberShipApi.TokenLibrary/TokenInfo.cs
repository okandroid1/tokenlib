﻿using System;

namespace BasarMemberShipApi.TokenLibrary
{
    public class TokenInfo
    {
        public int Id { get; set; }
        public ShortGuid Token { get; set; }
        public int Duration { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public DateTime CreatedDate { get; set; }
    }
}
