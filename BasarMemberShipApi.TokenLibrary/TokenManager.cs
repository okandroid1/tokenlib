﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasarMemberShipApi.TokenLibrary
{
    public static class TokenManager
    {

        // private static readonly TokenOperations t = new TokenOperations(new MongoOperations());
        private static readonly TokenOperations t;

        static TokenManager()
        {
            //t = new TokenOperations(new SqlOperations());
        }

        public static TokenOperations Get()
        {
            return TokenManager.t;
        }
    }
}
