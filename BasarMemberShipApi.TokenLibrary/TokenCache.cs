﻿using System;
using System.Runtime.Caching;

namespace BasarMemberShipApi.TokenLibrary
{
    public class TokenCache
    {
        MemoryCache mCache = MemoryCache.Default;

        public void AddCache(TokenInfo tokeninfo)
        {
            mCache.Add(tokeninfo.Token.ToString(), tokeninfo, DateTimeOffset.Now.AddMinutes(tokeninfo.Duration));
        }

        public TokenInfo GetCache(string token)
        {
            CacheItem cItemToken = mCache.GetCacheItem(token);
            return cItemToken != null ? (TokenInfo)cItemToken.Value : null;
        }
    }
}
