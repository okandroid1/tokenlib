﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BasarMemberShipApi.TokenLibrary
{
    public class TokenOperations
    {
        private readonly TokenCache mTokenCache = new TokenCache();

        private IDbOperations<TokenInfo> _dbOperations;

        public TokenOperations(IDbOperations<TokenInfo> dbOperations)
        {
            _dbOperations = dbOperations;
        }

        public TokenInfo GetToken(string usr, string pwd, string appType, int expDate)
        {

            if (_dbOperations.ValidateUser(usr, pwd))
            {
                TokenInfo tokenInfo = new TokenInfo();
                tokenInfo.UserName = usr;
                tokenInfo.Token = ShortGuid.NewGuid();
                tokenInfo.Duration = expDate;
                tokenInfo.CreatedDate = DateTime.Now;
                mTokenCache.AddCache(tokenInfo);
                _dbOperations.AddToken(tokenInfo);
                return tokenInfo;
            }
            else
            {
                return null;
            }

        }

        public TokenInfo IsAuth(string token)
        {
            return mTokenCache.GetCache(token);
        }

        public void AddCacheFromDB()
        {
            var tkList = _dbOperations.GetTokenList();

            foreach (var item in tkList)
            {
                TokenInfo tkinfo = new TokenInfo
                {
                    Token = item.Token.ToString(),
                    UserName = item.UserName,
                    Duration = item.Duration,
                    CreatedDate = item.CreatedDate
                };

                // Şu anki zamandan oluşturulma zamanını çıkartıyorum.
                // Fark duration'dan küçükse IIS kapandığında o token cache'te var demektir.
                TimeSpan fark = DateTime.Now.Subtract(tkinfo.CreatedDate);
                int dakika = fark.Minutes;

                if (dakika < tkinfo.Duration)
                {
                    mTokenCache.AddCache(tkinfo);
                }
            }
        }
    }
}
